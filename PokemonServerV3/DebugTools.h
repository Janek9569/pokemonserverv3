#pragma once
#include <stdarg.h>
#include <stdio.h>
#include <cstdint>
#include <string>
#include "SFML\Network.hpp"

void dbg_printf(const char *fmt, ...);
std::string dbg_get_server_command_str(sf::Packet pack);