#pragma once
#include "SFML\System.hpp"
#include "ConfigReader.h"
#include "PacketHandler.h"
#include "EventHandler.h"
#include "Environment.h"
#include "Cli.h"

class Server
{
public:
	Server();
	~Server();

	void run() { isEnabled = true; }
	void suspend() { isEnabled = false; }
	void terminate() { isTermination = true; while (isTermination); isEnabled = false; }
	bool isServerEnabled() const { return isEnabled; }

	EventHandler& getEventHandler() { return eventHandler; }
	ConfigReader& getConfigReader() { return cfg; }
	Environment& getEnv() { return env; }
	const EventHandler& getEventHandler() const { return eventHandler; }
	const ConfigReader& getConfigReader() const { return cfg; }
	const Environment& getEnv() const { return env; }
private:
	bool isEnabled;
	volatile bool isTermination;

	sf::Thread serverThread;
	void serverTask();

	ConfigReader cfg;
	Environment env;
	EventHandler eventHandler;
	PacketHandler packetHandler;
	Cli cli;
};

