#include "DatabaseManager.h"
#include <sstream>
#include <iostream>

DatabaseManager::DatabaseManager(std::unique_ptr<database> _db)
{
	db = std::move(_db);
}

DBResult_ptr DatabaseManager::getAllItemTypes()
{
	std::stringstream query;
	query << "SELECT* FROM ItemTypes ORDER BY ID ASC;";
	return db->executeAndStoreQuery(query.str());
}