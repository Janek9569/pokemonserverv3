#include "ConfigReader.h"
#include "XMLUtils.h"
#include "DebugTools.h"
#include "Defines.h"
#include "Utils.h"


ConfigReader::ConfigReader(const std::string& configFileName)
{
	dbg_printf("Loading server config File\n");

	ASSERT(parseDocument(doc, configFileName) == true);

	root = doc.first_node("serverConfig");
	ASSERT(root != nullptr);
}


ConfigReader::~ConfigReader()
{
}

XMLNode* ConfigReader::findNode(const char* name, XMLNode* parent = nullptr) const
{
	ASSERT(name != nullptr);
	if (!parent)
		parent = root;
	ASSERT(parent != nullptr);

	XMLNode* result = parent->first_node(name);
	ASSERT(result != nullptr);
	return result;
}

XMLAttribute* ConfigReader::findAttribute(const char* name, XMLNode* node) const
{
	ASSERT(name != nullptr);
	ASSERT(node != nullptr);

	XMLAttribute* result = node->first_attribute(name);
	ASSERT(result != nullptr);
	return result;
}

int ConfigReader::getTCPPort() const
{
	ASSERT(root != 0);

	XMLNode *TCPRoot = findNode("TCP");
	uint32_t port = atoi(findAttribute("port", TCPRoot)->value());
	dbg_printf("TCPPort read := %d\n", port);

	return port;
}

std::string ConfigReader::getMapFileName() const
{
	ASSERT(root != 0);

	XMLNode* mapRoot = findNode("Map");
	XMLAttribute* fileNameAttrib = findAttribute("fileDir", mapRoot);
	std::string result(fileNameAttrib->value());
	return result;
}

SizeParams2D ConfigReader::getMapSize() const
{
	ASSERT(root != 0);

	XMLNode* mapRoot = findNode("Map");
	XMLNode* mapSizeNode = findNode("MapSize", mapRoot);

	SizeParams2D result;
	result[0] = atoi(findAttribute("x", mapSizeNode)->value());
	result[1] = atoi(findAttribute("y", mapSizeNode)->value());
	dbg_printf("Map size read [x:= %d], [y:= %d]\n", result[0], result[1]);

	return result;
}

SizeParams3D ConfigReader::getChunkSize() const
{
	ASSERT(root != 0);

	XMLNode* mapRoot = findNode("Map");
	XMLNode* chunk = findNode("Chunk", mapRoot);

	SizeParams3D result;
	result[0] = atoi(findAttribute("x", chunk)->value());
	result[1] = atoi(findAttribute("y", chunk)->value());
	result[2] = atoi(findAttribute("z", chunk)->value());
	dbg_printf("Chunk size read [x:= %d], [y:= %d], [z:= %d]\n", result[0], result[1], result[2]);

	return result;
}

int ConfigReader::getMaxItemsOnMapPoint() const
{
	ASSERT(root != 0);

	XMLNode* mapRoot = findNode("Map");
	XMLNode* maxItemsOnMapPointNode = findNode("MaxItemsOnMapPoint", mapRoot);
	
	int result = atoi(findAttribute("value", maxItemsOnMapPointNode)->value());
	dbg_printf("Max items on map point read [%d]\n", result);

	return result;
}

std::string ConfigReader::getDatabaseDir() const
{
	ASSERT(root != 0);

	XMLNode* dbRoot = findNode("Database");

	std::string result = findAttribute("name", dbRoot)->value();
	dbg_printf("Database name read [%s]\n", result.c_str());

	return result;
}
