#include "Cli.h"
#include "DebugTools.h"
#include "Utils.h"

using namespace std::placeholders;

Cli::Cli(std::istream& input):
	_isEnd(false),
	in(input),
	cliThread(&Cli::cliTask, this)
{
	commands["/exit"] = std::bind(&Cli::exitProcedure, this);
	cliThread.launch();
}


Cli::~Cli()
{
}

void Cli::cliTask()
{
	std::string str = "";
	while (true)
	{
		std::cin >> str;
		dbg_printf("Cli calling %s command\n", str.c_str());
		commands[str]();
	}
}

void Cli::exitProcedure()
{
	_isEnd = true;
	cliThread.terminate();
}