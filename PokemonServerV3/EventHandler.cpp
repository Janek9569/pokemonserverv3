#include "EventHandler.h"
#include "DebugTools.h"

EventHandler::EventHandler()
{
	onPacketReceive.connect(&EventHandler::packetReceive, OnPacketReceive::HIGHEST_SLOT_PRIO);
}

EventHandler::~EventHandler()
{
}

void EventHandler::packetReceive(CommandsToServer cmd, size_t clientId, sf::Packet* packet)
{
	dbg_printf("OnPacketRecieve signal called\n");
}