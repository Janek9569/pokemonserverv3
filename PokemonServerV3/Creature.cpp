#include "Creature.h"
#include "DebugTools.h"


Creature::Creature():
	typeID(NO_CREATURE_TYPE_ID)
{
}


Creature::~Creature()
{
}

//typeID(4), //TODO: fill
void Creature::serialize(ByteVector & data) const
{
	dbg_printf("serializing creature [ID := %d]", typeID);
	const uint8_t* tempData = reinterpret_cast<const uint8_t*>(&typeID);
	data.insert(data.end(), tempData, tempData + sizeof(uint32_t));
}

const uint8_t* Creature::deserialize(const uint8_t* data)
{
	const uint32_t* creatureTypeID = reinterpret_cast<const uint32_t*>(data);
	data += sizeof(uint32_t);
	dbg_printf("Creature deserializing [ID := %d]\n", creatureTypeID);
	typeID = *creatureTypeID;

	if (typeID != NO_CREATURE_TYPE_ID)
	{
		//TODO: fill
	}

	return data;
}
