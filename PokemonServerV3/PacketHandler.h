#pragma once
#include <memory>
#include "SFML\Network.hpp"
#include "ThreadSafeDeque.h"
#include "Utils.h"
#include "TCPHandler.h"
#include "EventHandler.h"
#include <vector>

#define SIGNLAL_HANDLER_WAIT_TIME sf::milliseconds(5)

using ServerCommandFunctionType = void(size_t, sf::Packet*);
using ServerCommandFunction = std::function<ServerCommandFunctionType>;

struct PacketSignal
{
	PacketSignal() = delete;

	PacketSignal(size_t id, Packet_ptr& _packet): 
		id(id),
		packet(std::move(_packet))
	{}
	size_t id;
	Packet_ptr packet;
};

sf::Packet& operator >> (sf::Packet& packet, CommandsToServer& cmd);

class Server;

class PacketHandler
{
public:
	PacketHandler(uint32_t port, Server* server);
	~PacketHandler();
	void push_received(size_t id, Packet_ptr packet);

private:
	ThreadSafeDeque<PacketSignal> packetSignals_received;

	Server* server;
	sf::Thread signalHandlerThread;
	void signalReceiverTask();
	TCPHandler tcpHandler;

	std::vector<ServerCommandFunction> commandFunctions;
	void COMMAND_TO_SERVER_NONE_handle(size_t clientId, sf::Packet* packet);
	void COMMAND_TO_SERVER_MOVE_handle(size_t clientId, sf::Packet* packet);
	void COMMAND_TO_SERVER_LOGIN_handle(size_t clientId, sf::Packet* packet);
	void COMMAND_TO_SERVER_LOGOUT_handle(size_t clientId, sf::Packet* packet);
	void COMMAND_TO_SERVER_GET_CHARACTER_LIST_handle(size_t clientId, sf::Packet* packet);
	void COMMAND_TO_SERVER_DISCONNECT_handle(size_t clientId, sf::Packet* packet);
	void COMMAND_TO_SERVER_DISCONNECT_ACCOUNT_handle(size_t clientId, sf::Packet* packet);
	void COMMAND_TO_SERVER_IM_STILL_THERE_handle(size_t clientId, sf::Packet* packet);
	void COMMAND_TO_SERVER_GET_CHUNK_handle(size_t clientId, sf::Packet* packet);

	//EVENTS
	void handleCommandProcedure(CommandsToServer cmd, size_t clientId, sf::Packet* packet);
};
