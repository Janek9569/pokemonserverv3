#include "TCPHandler.h"
#include "PacketHandler.h"
#include <iostream>
#include <memory>
#include "DebugTools.h"


TCPHandler::TCPHandler(uint32_t port):
	port(port),
	listenerThread(&TCPHandler::listenerTask, this),
	selectorThread(&TCPHandler::slectorTask, this),
	zombieDetectorThread(&TCPHandler::zombieDetectionTask, this),
	packetHandler(nullptr)
{

	listenerThread.launch();
	selectorThread.launch();
	zombieDetectorThread.launch();
}


TCPHandler::~TCPHandler()
{
	listenerThread.terminate();
	selectorThread.terminate();
	zombieDetectorThread.terminate();
}

void TCPHandler::listenerTask()
{
	ASSERT(listener.listen(port) == sf::Socket::Done);

	while (true)
	{
		TCPActiveClient_ptr newClient = std::make_unique<TCPActiveClient>();
		if (listener.accept(*newClient) != sf::Socket::Done)
		{
			std::cout << "Error while accepting client connection" << std::endl;
		}
		else
		{
			dbg_printf("New client connected [id:=%d]\n", newClient->getId());
			selector.add(*newClient);
			newClient->resetClock();
			connectedClients.push_back(std::move(newClient));
		}
	}
}

void TCPHandler::slectorTask()
{
	while (true)
	{
		if (selector.wait(SELECTOR_WAIT_TIME))
		{
			for (size_t i = 0; i < connectedClients.size(); ++i)
			{
				auto& client = *connectedClients[i];
				Packet_ptr newPacket = std::make_unique<sf::Packet>();
				if (selector.isReady(client))
				{
					sf::Socket::Status recStatus = client.receive(*newPacket);
					if(recStatus == sf::Socket::Done)
					{
						dbg_printf("Received packet from client [id:=%d] [cmd:=%s]\n", client.getId(), dbg_get_server_command_str(*newPacket).c_str());
						client.resetClock();
						packetHandler->push_received(client.getId(), std::move(newPacket));
					}
				}
			}
		}
	}
}

void TCPHandler::zombieDetectionTask()
{
	while (true)
	{
		//check if exist client with timeout
		connectedClients.erase_if([this](TCPActiveClient_ptr& c) -> bool {
			if (c->checkTimeout())
			{
				selector.remove(*c);
				dbg_printf("Zombie client detected [id:=%d]\n", c->getId());
				return true;
			}
			return false;
		});
		sf::sleep(ZOMBIE_DETECTION_PERIOD);
	}
}