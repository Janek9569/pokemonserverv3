#pragma once
#include <map>
#include "UniqueObject.h"
#include "ThreadSafeVector.h"
#include "DatabaseManager.h"
#include "Creature.h"
#include "Item.h"
#include "Map.h"
#include "Item_t.h"

using UniqueObjectVector = ThreadSafeVector<UniqueObject_ptr>;
using Item_t_ptr = std::unique_ptr<Item_t>;
using NameToIDMap = std::map<std::string, uint32_t>;

class Server;

class Environment
{
public:
	Environment(Server* srv);
	~Environment();

	Creature* createCreature();
	Item* createItem(uint32_t ID, uint32_t typeID, uint16_t count);
	Item* createItem();

	Map& getMap() { return map; }
	const Map& getMap() const { return map; }

	UniqueObject* getObject(uint32_t ID);
	const Item_t* getItemType(uint32_t ID);
private:
	DatabaseManager dbm;
	uint32_t IDCounter;
	Server* srv;
	UniqueObjectVector objects;

	std::vector<Item_t_ptr> itemTypes;
	
	Map map;

	uint32_t getNewID();
	void loadFromDatabase();
	void loadItemsTypesFromDatabase();
};

