#pragma once

#include <cstdint>
#include <string>
#include "SizeParams.h"

#pragma pack(push, 1) // exact fit - no padding
struct ItemTypeAttributes
{
	unsigned int _canStepIn : 1;
	unsigned int _useable : 1;
	unsigned int _moveable : 1;
	unsigned int _tradeable : 1;
	unsigned int _pushable : 1;
	unsigned int _stackable : 1;
};
#pragma pack(pop)

class Item_t
{
public:
	Item_t(	uint32_t ID, 
			const SizeParams2D& size, 
			const std::string& name, 
			const std::string& desc,
			const ItemTypeAttributes& attr);
	~Item_t();

	const SizeParams2D& getSize() const { return size; }
	uint32_t getID() const { return ID; }
	const std::string& getName() const { return name; }
	const std::string& getDescription() const { return description; }
	const ItemTypeAttributes& getAttribs() const { return attribs; }
private:
	uint32_t ID;
	SizeParams2D size;
	std::string name;
	std::string description;
	ItemTypeAttributes attribs;
};

