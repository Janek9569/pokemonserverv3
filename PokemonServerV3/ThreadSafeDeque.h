#pragma once
#include <deque>
#include <SFML\System.hpp>
#include <algorithm>
#include "Utils.h"

template<typename T>
class ThreadSafeDeque
{
public:
	void push_back(T& t) { deq.push_back(t); }
	void push_front(T& t) { deq.push_front(t); }
	void push_back(T&& t) { deq.push_back(std::move(t)); }
	void push_front(T&& t) { deq.push_front(std::move(t)); }
	T& operator[](size_t ind) { return deq[ind]; }
	const T& operator[](size_t ind) const { return deq[ind]; }

	T pop_back() 
	{
		sf::Lock lock(mutex);
		ASSERT(deq.size() > 0);
		T temp = std::move(deq.back());
		deq.pop_back();
		return std::move(temp);
	}

	T pop_front()
	{
		sf::Lock lock(mutex);
		ASSERT(deq.size() > 0);
		T temp = std::move(deq.front());
		deq.pop_front();
		return std::move(temp);
	}

	size_t size()
	{
		sf::Lock lock(mutex);
		return deq.size();
	}

	template<class Function>
	Function for_each(Function fn)
	{
		sf::Lock lock(mutex);
		return std::for_each(deq.begin(), deq.end(), fn);
	}

	template<class Function>
	Function for_each(Function fn) const
	{
		sf::Lock lock(mutex);
		return std::for_each(deq.begin(), deq.end(), fn);
	}

	template<class cmp>
	void sort(cmp compare)
	{
		sf::Lock lock(mutex);
		std::sort(deq.begin(), deq.end(), compare);
	}

	template<class Function>
	void erase_if(Function fn)
	{
		sf::Lock lock(mutex);
		deq.erase(
			std::remove_if(
				deq.begin(),
				deq.end(),
				fn
			),
			deq.end()
		);
	}
private:
	sf::Mutex mutex;
	std::deque<T> deq;
};

