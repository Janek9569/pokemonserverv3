#include "PacketHandler.h"
#include "Defines.h"
#include "DebugTools.h"
#include "Utils.h"
#include "Server.h"
using namespace std::placeholders;
PacketHandler::PacketHandler(uint32_t port, Server* server):
	server(server),
	signalHandlerThread(&PacketHandler::signalReceiverTask, this),
	tcpHandler(port),
	commandFunctions(COMMAND_TO_SERVER_LAST)
{
	commandFunctions[COMMAND_TO_SERVER_NONE] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_MOVE] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_LOGIN] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_LOGOUT] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_GET_CHARACTER_LIST] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_DISCONNECT] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_DISCONNECT_ACCOUNT] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_IM_STILL_THERE] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_GET_CHUNK] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);
	commandFunctions[COMMAND_TO_SERVER_DISCONNECT_ACCOUNT] = std::bind(&PacketHandler::COMMAND_TO_SERVER_NONE_handle, this, _1, _2);

	tcpHandler.attachPacketHandler(this);

	std::function<void(CommandsToServer cmd, size_t clientId, sf::Packet* packet)> 
		allCommandHandler = std::bind(&PacketHandler::handleCommandProcedure, this, _1, _2, _3);

	server->getEventHandler().onPacketReceive.connect(allCommandHandler, OnPacketReceive::LOWEST_SLOT_PRIO);
	signalHandlerThread.launch();
}


PacketHandler::~PacketHandler()
{
	signalHandlerThread.terminate();
}

void PacketHandler::push_received(size_t id, Packet_ptr packet)
{
	packetSignals_received.push_back(PacketSignal(id, packet));
}

void PacketHandler::signalReceiverTask()
{
	while (true)
	{
		if (packetSignals_received.size())
		{
			PacketSignal ps = packetSignals_received.pop_front();
			sf::Packet& packet = *ps.packet;
			size_t clientID = ps.id;
			CommandsToServer actCommand;
			packet >> actCommand;
			dbg_printf("Signal recieved [client:=%d], [command:=%s]\n", clientID, enum2str(actCommand));
			server->getEventHandler().onPacketReceive.activate(actCommand, clientID, &packet);
		}
		else
		{
			sf::sleep(SIGNLAL_HANDLER_WAIT_TIME);
		}
	}
}

void PacketHandler::handleCommandProcedure(CommandsToServer cmd, size_t clientId, sf::Packet* packet)
{
	ASSERT(cmd < COMMAND_TO_SERVER_LAST);
	commandFunctions[cmd](clientId, packet);
}

sf::Packet& operator >> (sf::Packet& packet, CommandsToServer& cmd)
{
	uint32_t val;
	packet >> val;
	ASSERT(cmd < COMMAND_TO_SERVER_LAST);
	cmd = static_cast<CommandsToServer>(val);
	return packet;
}

void PacketHandler::COMMAND_TO_SERVER_NONE_handle(size_t clientId, sf::Packet* packet)
{
	dbg_printf("COMMAND_TO_SERVER_NONE command called ?!\n");
}

void PacketHandler::COMMAND_TO_SERVER_IM_STILL_THERE_handle(size_t clientId, sf::Packet* packet)
{
	return; // do nothing
}

void PacketHandler::COMMAND_TO_SERVER_MOVE_handle(size_t clientId, sf::Packet* packet)
{
	//TODO: fill
}

void PacketHandler::COMMAND_TO_SERVER_LOGIN_handle(size_t clientId, sf::Packet* packet)
{
	//TODO: fill
}

void PacketHandler::COMMAND_TO_SERVER_LOGOUT_handle(size_t clientId, sf::Packet* packet)
{
	//TODO: fill
}

void PacketHandler::COMMAND_TO_SERVER_GET_CHARACTER_LIST_handle(size_t clientId, sf::Packet* packet)
{
	//TODO: fill
}

void PacketHandler::COMMAND_TO_SERVER_DISCONNECT_handle(size_t clientId, sf::Packet* packet)
{
	//TODO: fill
}

void PacketHandler::COMMAND_TO_SERVER_DISCONNECT_ACCOUNT_handle(size_t clientId, sf::Packet* packet)
{
	//TODO: fill
}

void PacketHandler::COMMAND_TO_SERVER_GET_CHUNK_handle(size_t clientId, sf::Packet* packet)
{
	//TODO: fill
}