#pragma once
#include <cstdint>
#include <vector>
#include "Defines.h"
#include "ThreadSafeDeque.h"
#include "SizeParams.h"
#include "Item.h"
#include "Creature.h"

class Server;
class MapPoint;
class Chunk;

using MapPoint3DVect = std::vector<std::vector<std::vector<MapPoint*>>>;
using Chunk2DVect = std::vector<std::vector<Chunk>>;

class MapPoint
{
public:
	MapPoint(Server* srv);
	void serialize(ByteVector& data);
	const uint8_t* deserialize(const uint8_t* data);
	auto& getItems() { return items; }
	const auto& getItems() const { return items; }
	const Creature* getCreature() const { return creature; }
	void setCreatre(Creature* c)
	{
		sf::Lock lock(mutex);
		creature = c;
	}
private:
	Server* srv;
	sf::Mutex mutex;
	//front - top, back - bottom
	ThreadSafeDeque<Item*> items;
	Creature* creature;
};

class Chunk
{
public:
	Chunk(const uint8_t* data, Server* srv, const uint8_t** actData);
	Chunk(SizeParams3D& sizeParams, Server* srv);
	~Chunk();
	void serialize(ByteVector& data);
	auto& operator[](size_t index) { return chunk[index]; }
	const auto& operator[](size_t index) const { return chunk[index]; }
	template<class F>
	void for_each_point(F f)
	{
		for (auto& chZ : chunk)
		{
			for (auto& chY : chZ)
			{
				for (auto& chX : chY)
				{
					f(chX);
				}
			}
		}
	}

private:
	uint8_t sizeX;
	uint8_t sizeY;
	uint8_t sizeZ;
	//[z][y][x]
	MapPoint3DVect chunk;
	bool _isInitialised;
	Server* srv;
};

class Map
{
public:
	Map(Server* server);
	~Map();

	auto& operator[](size_t index) { return chunks[index]; }
	const auto& operator[](size_t index) const { return chunks[index]; }

	void saveMapToFile();

private:
	Server* server;
	//x-chunks, y-chunks, z-levels
	const SizeParams2D mapSizeVector;
	const int maxItemsOnMapPoint;
	//[y][x]
	Chunk2DVect chunks;

	sf::Thread mapSaveThread;
	void mapSavingToFileProcedure();

	void loadMapFromFile();

	template<class F>
	void for_each_chunk(F f)
	{
		for (auto& chY : chunks)
		{
			for (auto& chX : chY)
			{
				f(chX);
			}
		}
	}

	template<class F>
	void for_each_chunk(F f) const
	{
		for (auto& chY : chunks)
		{
			for (auto& chX : chY)
			{
				f(chX);
			}
		}
	}
};

