#include "Server.h"
#include <iostream>
#include <string>


Server::Server() :
	isEnabled(false),
	serverThread(&Server::serverTask, this),
	isTermination(false),
	cfg(SERVER_CONFIG_FILE),
	packetHandler(cfg.getTCPPort(), this),
	cli(std::cin),
	env(this)
{
	serverThread.launch();
}


Server::~Server()
{
	serverThread.terminate();
}

void Server::serverTask()
{
	while ( !cli.isEnd() )
	{

	}
	dbg_printf("Server terminated\n");
	isEnabled = false;
	serverThread.terminate();
}