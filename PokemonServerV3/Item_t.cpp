#include "Item_t.h"



Item_t::Item_t(uint32_t ID, const SizeParams2D& size, const std::string& name,
	const std::string& desc, const ItemTypeAttributes& attr):
	ID(ID),
	size(size),
	name(name),
	description(desc),
	attribs(attr)
{
}


Item_t::~Item_t()
{
}
