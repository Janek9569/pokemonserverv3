#pragma once
#include "SFML\Network.hpp"
#include "SFML\System.hpp"
#include "DebugTools.h"

#define TIMEOUT_VALUE sf::seconds(50)

class TCPActiveClient : public sf::TcpSocket
{
public:
	TCPActiveClient();
	~TCPActiveClient();

	size_t getId() const { return id; }

	bool checkTimeout() const 
	{ 
		bool result = clock.getElapsedTime() > TIMEOUT_VALUE; 
		return result;
	}

	void resetClock() { clock.restart(); }

private:
	size_t id;
	sf::Clock clock;
};

