#pragma once
#include <iostream>
#include <map>
#include <functional>
#include "SFML\System.hpp"


class Cli
{
public:
	Cli(std::istream& input);
	~Cli();

	bool isEnd() const { return _isEnd; }
private:
	std::map<std::string, std::function<void(void)>> commands;

	bool _isEnd;
	std::istream& in;

	sf::Thread cliThread;
	void cliTask();

	void exitProcedure();
};

