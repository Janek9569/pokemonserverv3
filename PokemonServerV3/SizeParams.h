#pragma once
#include <vector>
#include "Defines.h"

class SizeParams2D
{
public:
	SizeParams2D() : data(DIMS) {}
	SizeParams2D(int sizeX, int sizeY) : 
		data(DIMS) 
	{ 
		data[_X] = sizeX;
		data[_Y] = sizeY; 
	}

	int getX() const { return data[_X]; }
	int getY() const { return data[_Y]; }
	void setX(int newVal) { data[_X] = newVal; }
	void setY(int newVal) { data[_Y] = newVal; }

	auto& operator[](size_t index) { return data[index]; }
	const auto& operator[](size_t index) const { return data[index]; }
private:
	std::vector<int> data;
	static constexpr uint8_t DIMS = 2;
};

class SizeParams3D
{
public:
	SizeParams3D() : data(DIMS) {}
	int getX() const { return data[_X]; }
	int getY() const { return data[_Y]; }
	int getZ() const { return data[_Z]; }
	void setX(int newVal) { data[_X] = newVal; }
	void setY(int newVal) { data[_Y] = newVal; }
	void setZ(int newVal) { data[_Z] = newVal; }

	auto& operator[](size_t index) { return data[index]; }
	const auto& operator[](size_t index) const { return data[index]; }
private:
	std::vector<int> data;
	static constexpr uint8_t DIMS = 3;
};