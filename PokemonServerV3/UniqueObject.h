#pragma once
#include <memory>
#include <cstdint>

class Server;
class UniqueObject;
using UniqueObject_ptr = std::unique_ptr<UniqueObject>;

class UniqueObject
{
public:
	UniqueObject();
	~UniqueObject();

	uint32_t getID() const { return ID; }
	void setID(uint32_t newID);

	static constexpr uint32_t NO_ID = 0xFFFFFFFFUL;

private:
	uint32_t ID;
};

