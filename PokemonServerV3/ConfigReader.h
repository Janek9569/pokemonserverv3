#pragma once
#include <string>
#include <vector>
#include "XMLUtils.h"
#include "Defines.h"
#include "SizeParams.h"

class ConfigReader
{
public:
	ConfigReader(const std::string& configFileName = SERVER_CONFIG_FILE);
	~ConfigReader();

	/*****TCP*****/
	int getTCPPort() const;

	/*****MAP*****/
	std::string getMapFileName() const;
	//[0]->x, [1]->y
	SizeParams2D getMapSize() const;
	//[0]->x, [1]->y, [2]->z
	SizeParams3D getChunkSize() const;
	int getMaxItemsOnMapPoint() const;

	/*****DATABASE*****/
	std::string getDatabaseDir() const;

private:
	XMLNode* findNode(const char* name, XMLNode* parent) const;
	XMLAttribute* findAttribute(const char* name, XMLNode* node) const;

	XMLDoc doc;
	XMLNode* root;
};

