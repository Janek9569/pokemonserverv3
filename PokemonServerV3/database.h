#pragma once
#include "sqlite3.h"
#include <memory>
#include <string>
#include <mutex>
#include <map>

class DBReusltInterpeter;

//singleton class
class database
{
public:
	//this can't return any result
	bool executeQuery(std::string query);
	std::unique_ptr<DBReusltInterpeter> executeAndStoreQuery(std::string query);
	~database();
	database(const std::string& dbName);

	bool beginTransaction() { return executeQuery(std::string("BEGIN")); }
	bool rollback() { return executeQuery(std::string("ROLLBACK")); }
	bool commit() { return executeQuery(std::string("COMMIT")); }

private:
	bool isDBConnected;

	sqlite3* db;
	std::string parse(const std::string& s);
	std::recursive_mutex dbMutex;
};

class DBReusltInterpeter
{
public:
	int32_t getDataInt(const std::string &s);
	int64_t getDataLong(const std::string &s);
	std::string getDataString(const std::string &s);
	const char* getDataStream(const std::string &s, size_t* size = nullptr);
	bool next();

	DBReusltInterpeter(sqlite3_stmt* stmt);
	~DBReusltInterpeter() { free(); };

private:
	void free();
	typedef std::map<const std::string, uint32_t> ColumnNames;
	ColumnNames columnNames;
	sqlite3_stmt* stmt;
};