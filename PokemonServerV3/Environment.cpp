#include "Environment.h"
#include "Utils.h"
#include "DebugTools.h"
#include "Server.h"
#include <memory>

Environment::Environment(Server* srv) :
	dbm(std::make_unique<database>(srv->getConfigReader().getDatabaseDir())),
	IDCounter(0),
	srv(srv),
	map(srv)
{
	loadFromDatabase();
}

Environment::~Environment()
{
}

Creature* Environment::createCreature()
{
	Creature_ptr newCreature = std::make_unique<Creature>();
	Creature* result = newCreature.get();
	newCreature->setID(getNewID());
	objects.push_back(std::move(newCreature));
	dbg_printf("New Creature created [ID := %d]\n", newCreature->getID());
	return result;
}

Item* Environment::createItem(uint32_t ID, uint32_t typeID, uint16_t count)
{
	Item_ptr newItem = std::make_unique<Item>(srv, ID, typeID, count);
	Item* result = newItem.get();
	newItem->setID(getNewID());
	objects.push_back(std::move(newItem));
	dbg_printf("New Item created [ID := %d]\n", newItem->getID());
	return result;
}

Item* Environment::createItem()
{
	Item_ptr newItem = std::make_unique<Item>();
	Item* result = newItem.get();
	newItem->setID(getNewID());
	objects.push_back(std::move(newItem));
	dbg_printf("New Item created [ID := %d]\n", newItem->getID());
	return result;
}

UniqueObject* Environment::getObject(uint32_t ID)
{
	ASSERT(ID < objects.size());
	return objects[ID].get();
}

const Item_t* Environment::getItemType(uint32_t ID)
{
	ASSERT(ID < itemTypes.size());
	return itemTypes[ID].get();
}

uint32_t Environment::getNewID()
{
	uint32_t res = IDCounter++; 
	return res;
}

void Environment::loadFromDatabase()
{
	loadItemsTypesFromDatabase();
}

void Environment::loadItemsTypesFromDatabase()
{
	dbg_printf("Loading item types from database\n");
	DBResult_ptr dbRes = dbm.getAllItemTypes();
	uint32_t IDcheck = 0;
	while (dbRes->next())
	{
		uint32_t ID = dbRes->getDataInt("ID");
		ASSERT(ID == IDcheck++);
		int32_t sizeX = dbRes->getDataInt("sizeX");
		int32_t sizeY = dbRes->getDataInt("sizeY");
		std::string name = dbRes->getDataString("name");
		std::string desc = dbRes->getDataString("desc");
		const ItemTypeAttributes* attribs = 
			reinterpret_cast<const ItemTypeAttributes*>(dbRes->getDataStream("attr"));
		
		dbg_printf("  ItemTypeRegistered [ID := %d] [name := %s]\n", ID, name.c_str());

		itemTypes.push_back(
			std::make_unique<Item_t>(
				ID, SizeParams2D(sizeX, sizeY), name, desc, *attribs
			)
		);
	}
}
