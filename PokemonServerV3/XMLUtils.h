#pragma once
#include <cstdint>
#include <string>
#include <cstdlib>
#include "rapidxml.hpp"
#include "rapidxml_iterators.hpp"
#include "rapidxml_utils.hpp"
#include "rapidxml_print.hpp"

using XMLDoc = rapidxml::xml_document<char>;
using XMLNode = rapidxml::xml_node<char>;
using XMLAttribute = rapidxml::xml_attribute<char>;

bool parseDocument(XMLDoc& doc, std::string dir);