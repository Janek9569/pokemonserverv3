#include "DebugTools.h"
#include "PacketHandler.h"

void dbg_printf(const char *fmt, ...)
{
#ifdef _DEBUG
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
#else
	(void)fmt;
#endif
}

std::string dbg_get_server_command_str(sf::Packet pack)
{
#ifdef _DEBUG
	uint32_t cmd = 0;
	pack >> cmd;
	CommandsToServer srvCommand = (CommandsToServer)cmd;
	return enum2str(srvCommand);
#endif
}