#include "Utils.h"

std::string splitFilename(const std::string & str)
{
	size_t found;
	found = str.find_last_of("/\\");
	return str.substr(0, found);
}

std::streampos getFileSize(std::fstream& f)
{
	ASSERT(f.is_open() == true);
	std::streampos fsize = 0;

	fsize = f.tellg();
	f.seekg(0, std::ios::end);
	fsize = f.tellg() - fsize;

	return fsize;
}