#include "database.h"
#include "DebugTools.h"
#include "Utils.h"
#include <iostream>
#include <fstream>
#include <sstream>

database::database(const std::string& dbName)
{
	isDBConnected = false;
	char* errMsg = nullptr;
	int rc;

	rc = sqlite3_open(dbName.c_str(), &db);
	ASSERT(rc == SQLITE_OK);
	isDBConnected = true;
	executeQuery("VACUUM;");
	executeQuery("PRAGMA journal_mode = `TRUNCATE`;");
}

bool database::executeQuery(std::string query)
{
	dbMutex.lock();

	if (!isDBConnected)
	{
		dbMutex.unlock();
		return false;
	}

	std::string buf = parse(query);
	sqlite3_stmt* stmt;
	if (sqlite3_prepare_v2(db, buf.c_str(), (int)buf.length(), &stmt, NULL) 
		!= SQLITE_OK)
	{
		sqlite3_finalize(stmt);
		dbg_printf("Can not prepare databse [%s]\n", sqlite3_errmsg(db));
		dbMutex.unlock();
		return false;
	}

	int ret = sqlite3_step(stmt);

	if (ret != SQLITE_OK && ret != SQLITE_DONE && ret != SQLITE_ROW)
	{
		sqlite3_finalize(stmt);
		dbg_printf("Can not perform databse step [%s]\n", sqlite3_errmsg(db));
		dbMutex.unlock();
		return false;
	}

	sqlite3_finalize(stmt);

	dbMutex.unlock();
	return true;
}

std::unique_ptr<DBReusltInterpeter> database::executeAndStoreQuery(std::string query)
{
	dbMutex.lock();

	if (!isDBConnected)
	{
		dbMutex.unlock();
		return false;
	}

	std::string buf = parse(query);
	sqlite3_stmt* stmt;

	if (sqlite3_prepare_v2(db, buf.c_str(), (int)buf.length(), &stmt, NULL) != SQLITE_OK)
	{
		sqlite3_finalize(stmt);
		dbg_printf("Can not prepare databse [%s]\n", sqlite3_errmsg(db));

		dbMutex.unlock();
		return nullptr;
	}

	std::unique_ptr<DBReusltInterpeter> pResult = std::make_unique<DBReusltInterpeter>(stmt);

	dbMutex.unlock();
	return pResult;
}

database::~database()
{
	sqlite3_close(db);
}

std::string database::parse(const std::string &s)
{
	std::string query = "";
	query.reserve(s.size());

	bool inString = false;
	for (uint32_t i = 0; i < s.length(); ++i)
	{
		uint8_t ch = s[i];
		if (ch == '\'')
		{
			if (inString && s[i + 1] != '\'')
				inString = false;
			else
				inString = true;
		}

		if (ch == '`' && !inString)
			ch = '"';

		query += ch;
	}

	return query;
}

DBReusltInterpeter::DBReusltInterpeter(sqlite3_stmt* stmt)
{
	if (!stmt)
		return;

	this->stmt = stmt;
	columnNames.clear();

	int32_t columns = sqlite3_column_count(this->stmt);

	for (int32_t i = 0; i < columns; ++i)
		columnNames[sqlite3_column_name(this->stmt, i)] = i;
}

int32_t DBReusltInterpeter::getDataInt(const std::string &s)
{
	ColumnNames::iterator it = columnNames.find(s);
	if (it != columnNames.end())
		return sqlite3_column_int(stmt, it->second);

	dbg_printf("getDataInt failed\n");
	return 0;
}

int64_t DBReusltInterpeter::getDataLong(const std::string &s)
{
	ColumnNames::iterator it = columnNames.find(s);
	if (it != columnNames.end())
		return sqlite3_column_int64(stmt, it->second);

	dbg_printf("getDataLong failed\n");
	return 0;
}

std::string DBReusltInterpeter::getDataString(const std::string &s)
{
	ColumnNames::iterator it = columnNames.find(s);
	if (it != columnNames.end())
	{
		if (sqlite3_column_type(stmt, it->second) == SQLITE_NULL)
		{
			return "";
		}
		std::string value = (const char*)sqlite3_column_text(stmt, it->second);
		return value;
	}

	dbg_printf("getDataString failed\n");
	return std::string();
}

const char* DBReusltInterpeter::getDataStream(const std::string &s, size_t* size)
{
	ColumnNames::iterator it = columnNames.find(s);
	if (it != columnNames.end())
	{
		const char* value = (const char*)sqlite3_column_blob(stmt, it->second);
		if(size)
			*size = sqlite3_column_bytes(stmt, it->second);
		return value;
	}

	dbg_printf("getDataStream failed\n");
	return nullptr;
}

bool DBReusltInterpeter::next()
{
	return sqlite3_step(stmt) == SQLITE_ROW;
}

void DBReusltInterpeter::free()
{
	if (stmt)
		sqlite3_finalize(stmt);
	else
		dbg_printf("Attempt to free null pointer\n");
}