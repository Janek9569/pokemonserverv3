#include "XMLUtils.h"
#include "DebugTools.h"
#include <iostream>
#include <sstream>

bool parseDocument(XMLDoc& doc, std::string dir)
{
	static std::string content;
	std::ifstream file(dir);
	if (!file.good())
	{
		dbg_printf("Can not read file %s\n", dir);
		return false;
	}
	std::stringstream buffer;
	buffer << file.rdbuf();
	file.close();
	content = buffer.str();
	try 
	{ 
		doc.parse<0>(&content[0]); 
	}
	catch (rapidxml::parse_error p)
	{
		dbg_printf("Can not parse xml file %s\n", dir.c_str());
		return false;
	}
	return true;
}
