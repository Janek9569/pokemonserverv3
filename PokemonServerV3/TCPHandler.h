#pragma once

#include "SFML/Network.hpp"
#include "SFML/System.hpp"
#include <vector>
#include "TCPActiveClient.h"
#include "ThreadSafeDeque.h"
#include <memory>

class PacketHandler;
#define ZOMBIE_DETECTION_PERIOD sf::seconds(10)
#define SELECTOR_WAIT_TIME sf::seconds(1)
typedef std::unique_ptr<TCPActiveClient> TCPActiveClient_ptr;
typedef std::unique_ptr<sf::Packet> Packet_ptr;

class TCPHandler
{
public:
	TCPHandler(uint32_t port);
	~TCPHandler();

	void attachPacketHandler(PacketHandler* handler) { packetHandler = handler; }
private:

	void listenerTask();
	sf::Thread listenerThread;
	sf::TcpListener listener;
	ThreadSafeDeque<TCPActiveClient_ptr> connectedClients;

	sf::SocketSelector selector;
	void slectorTask();
	sf::Thread selectorThread;

	PacketHandler* packetHandler;

	void zombieDetectionTask();
	sf::Thread zombieDetectorThread;

	uint32_t port;

};

