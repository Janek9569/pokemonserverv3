#pragma once
#include "Defines.h"
#include "SFML\System.hpp"
#include "SFML/Network.hpp"
#include <functional>
#include "ThreadSafeDeque.h"
#include <cstdint>

template<class T>
class Signal;

template<class Ret, class ...Args>
class Signal<Ret(Args...)>
{
public:
	
	class SlotType
	{
	public:
		
		SlotType(Ret(*func)(Args...)) : 
			prio(0), 
			f(func)
		{
			id = _id++;
		}

		SlotType(std::function<Ret(Args...)>& func) :
			prio(0),
			f(func)
		{
			id = _id++;
		}

		Ret call(Args... a) { f(a...); }
		
		bool operator==(SlotType& right) const
		{
			return this->id == right.id;
		}

		bool operator<(const SlotType& right) const
		{
			return this->prio < right.prio;
		}

		void setPrio(uint8_t newPrio) { prio = newPrio; }
		uint8_t getPrio() const { return prio; }
	private:
		static uint32_t _id;
		uint32_t id;
		uint8_t prio;
		std::function<Ret(Args...)> f;
	};
	
	void activate(Args... a)
	{
		activeSlots.for_each(
			[a...](SlotType& slot) {
			slot.call(a...);
		});
	}
	
	void connect(Ret(*newFunc)(Args...), uint8_t prio = LOWEST_SLOT_PRIO)
	{
		SlotType newSlot(newFunc);
		newSlot.setPrio(prio);
		activeSlots.push_back(newSlot);
		activeSlots.sort(
			[](const SlotType& left, const SlotType& right)->bool {
			return left < right;
		});
	}

	void connect(std::function<Ret(Args...)>& func, uint8_t prio = LOWEST_SLOT_PRIO)
	{
		SlotType newSlot(func);
		newSlot.setPrio(prio);
		activeSlots.push_back(newSlot);
		activeSlots.sort(
			[](const SlotType& left, const SlotType& right)->bool {
			return left < right;
		});
	}

	static constexpr uint8_t LOWEST_SLOT_PRIO = 0;
	static constexpr uint8_t HIGHEST_SLOT_PRIO = 0xFF;

private:
	ThreadSafeDeque<SlotType> activeSlots;
};

template<class Ret, class ...Args>
uint32_t Signal<Ret(Args...)>::SlotType::_id = 0;


typedef Signal<void(CommandsToServer cmd, size_t clientId, sf::Packet* packet)> OnPacketReceive;

class EventHandler
{
public:
	EventHandler();
	~EventHandler();

	OnPacketReceive onPacketReceive;

private:
	static void packetReceive(CommandsToServer cmd, size_t clientId, sf::Packet* packet);
};

