#include "UniqueObject.h"
#include "DebugTools.h"
#include "Utils.h"

UniqueObject::UniqueObject():
	ID(NO_ID)
{
}


UniqueObject::~UniqueObject()
{
}

void UniqueObject::setID(uint32_t newID)
{
	ASSERT(ID == NO_ID);
	ID = newID;
}