#include "Map.h"
#include "Server.h"
#include "DebugTools.h"
#include <fstream>

Map::Map(Server* server):
	server(server),
	mapSizeVector(server->getConfigReader().getMapSize()),
	maxItemsOnMapPoint(server->getConfigReader().getMaxItemsOnMapPoint()),
	mapSaveThread(&Map::mapSavingToFileProcedure, this)
{
	loadMapFromFile();
}


Map::~Map()
{
}

void Map::mapSavingToFileProcedure()
{
	std::string fileName = std::move(server->getConfigReader().getMapFileName());
	dbg_printf("Saving map to file %s\n", fileName.c_str());

	std::fstream mapFile(fileName, std::ios::binary | std::ios::out);
	ASSERT(mapFile.is_open() == true);

	ByteVector bytes;

	for_each_chunk(
		[&](Chunk& ch) {
		ch.serialize(bytes);
	});

	const char* dataToFile_ptr = reinterpret_cast<const char*>(bytes.data());
	mapFile.write(dataToFile_ptr, bytes.size());
	dbg_printf("Map saved successfully\n");
}

void Map::saveMapToFile()
{
	mapSaveThread.wait();
	mapSaveThread.launch();
}

void Map::loadMapFromFile()
{
	std::string fileName = std::move(server->getConfigReader().getMapFileName());
	dbg_printf("Reading map from file %s\n", fileName.c_str());

	std::fstream mapFile(fileName, std::ios::binary | std::ios::in);
	ASSERT(mapFile.is_open() == true);

	std::streampos fileSize = getFileSize(mapFile);
	ASSERT(fileSize > 0);
	uint8_t* fileData = new uint8_t[fileSize];
	mapFile.read(reinterpret_cast<char*>(fileData), fileSize);

	const uint8_t* actData = fileData;

	chunks.resize(mapSizeVector[_Y]);
	for (auto& mapX : chunks)
	{
		mapX.resize(mapSizeVector[_X],
			Chunk(actData, server, &actData));
	}

	delete[] fileData;
	dbg_printf("Map read successfully\n");
}

Chunk::Chunk(SizeParams3D& sizeParams, Server* srv):
	sizeX(sizeParams[_X]),
	sizeY(sizeParams[_Y]),
	sizeZ(sizeParams[_Z]),
	srv(srv)
{
	dbg_printf("Chunk::Chunk(SizeParams3D& sizeParams): should not be used\n");
	chunk.resize(sizeZ);
	for (auto& chZ : chunk)
	{
		chZ.resize(sizeY);

		for (auto& chY : chZ)
		{
			chY.resize(sizeX);
			for(auto& chX : chY)
				chX = new MapPoint(srv);
		}
	}
}

//sizeX(1), sizeY(1), sizeZ(1), [serialized MapPoint]
void Chunk::serialize(ByteVector& data)
{
	data.push_back(sizeX);
	data.push_back(sizeY);
	data.push_back(sizeZ);

	for_each_point(
		[&](MapPoint*& mp) {
		mp->serialize(data);
	});
}

Chunk::~Chunk()
{
	for_each_point(
		[&](MapPoint* mp) {
		delete mp;
	});
}

//deserialize
Chunk::Chunk(const uint8_t* data, Server* srv, const uint8_t** actData = nullptr) :
	sizeX(*data),
	sizeY(*(data + 1)),
	sizeZ(*(data + 2)),
	srv(srv)
{
	dbg_printf("Chunk deserializing size [x := %d], [y := %d], [z := %d]\n",
		sizeX, sizeY, sizeZ);

	chunk.resize(sizeZ);
	for (auto& chZ : chunk)
	{
		chZ.resize(sizeY);
		for (auto& chY : chZ)
		{
			chY.resize(sizeX);
			for(auto& chX : chY)
				chX = new MapPoint(srv);
		}
	}

	const uint8_t* _actData = data + 3;
	for_each_point(
		[&](MapPoint* mp) {
		_actData = mp->deserialize(_actData);
	});

	if (actData)
		*actData = _actData;
}

MapPoint::MapPoint(Server* srv) : 
	srv(srv)
{}

//serialized creature(4(nullptr) or more), itemCount(4), [serialized item]
void MapPoint::serialize(ByteVector& data)
{
	sf::Lock lock(mutex);
	dbg_printf("Serializing MapPoint\n");

	if(creature)
		creature->serialize(data);
	else
	{
		uint32_t noCreatureID = Creature::NO_CREATURE_TYPE_ID;
		uint8_t* noCreatureIDData =
			reinterpret_cast<uint8_t*>(&noCreatureID);
		data.insert(data.end(), noCreatureIDData, noCreatureIDData + sizeof(uint32_t));
	}

	uint32_t itemCount = (uint32_t)items.size();
	uint8_t* p = reinterpret_cast<uint8_t*>(&itemCount);
	data.insert(data.end(), p, p + sizeof(uint32_t));

	items.for_each(
		[&](Item*& item){
		item->serialize(data);
	});
}

const uint8_t* MapPoint::deserialize(const uint8_t* data)
{
	sf::Lock lock(mutex);

	const uint32_t* isCreature =
		reinterpret_cast<const uint32_t*>(data);
	if (*isCreature)
	{
		Creature* deserializedCreature = srv->getEnv().createCreature();
		this->creature = deserializedCreature;
		data = deserializedCreature->deserialize(data);
	}

	const uint32_t* itemCount = 
		reinterpret_cast<const uint32_t*>(data);
	dbg_printf("MapPoint deserializing [itemCount := %d]\n", *itemCount);
	data += sizeof(uint32_t);

	for (uint32_t i = 0; i < *itemCount; ++i)
	{
		Item* deserializedItem = srv->getEnv().createItem();
		data = deserializedItem->deserialize(srv, data);
		items.push_front(deserializedItem);
	}
	return data;
}
