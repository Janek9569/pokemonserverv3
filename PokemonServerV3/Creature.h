#pragma once
#include <memory>
#include "Defines.h"
#include "UniqueObject.h"

class Creature;
using Creature_ptr = std::unique_ptr<Creature>;

class Creature : public UniqueObject
{
public:
	Creature();
	~Creature();
	void serialize(ByteVector& data) const;
	const uint8_t* deserialize(const uint8_t* data);
	uint32_t getTypeID() const { return typeID; }

	static constexpr uint32_t NO_CREATURE_TYPE_ID = 0xFFFFFFFFUL;
private:
	uint32_t typeID;
};

