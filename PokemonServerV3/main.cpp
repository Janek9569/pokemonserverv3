#include <TGUI\TGUI.hpp>
#include "PacketHandler.h"
#include "ConfigReader.h"
#include "Server.h"

#include <string>
#include <iostream>

int main()
{
	Server server;
	server.run();

	while (server.isServerEnabled())
	{
		sf::sleep(sf::seconds(5));
	}
}