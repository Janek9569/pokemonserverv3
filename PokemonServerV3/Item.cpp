#include "Item.h"
#include "DebugTools.h"


Item::Item(Server* srv, uint32_t ID, uint32_t typeID, uint16_t count) : 
	srv(srv),
	ID(ID),
	typeID(typeID),
	count(count)
{
}


Item::~Item()
{
}

//ID(4), TODO: fill
void Item::serialize(ByteVector& data) const
{
	ASSERT(isInitialised());
	dbg_printf("serializing item [ID := %d]", typeID);
	const uint8_t* tempData = reinterpret_cast<const uint8_t*>(&typeID);
	data.insert(data.end(), tempData, tempData + sizeof(uint32_t));
}

const uint8_t* Item::deserialize(Server* _srv, const uint8_t* data)
{
	ASSERT(!isInitialised());
	srv = _srv;
	const uint32_t* itemTypeID = reinterpret_cast<const uint32_t*>(data);
	typeID = *itemTypeID;
	dbg_printf("Item deserializing [ID := %d]\n", typeID);
	return data + sizeof(uint32_t);
}

uint32_t Item::getTypeID() const
{
	ASSERT(isInitialised());
	return typeID;
}

uint16_t Item::getCount() const
{
	ASSERT(isInitialised());
	return count;
}

void Item::setCount(uint16_t newCount)
{
	ASSERT(isInitialised());
	count = newCount;
}

