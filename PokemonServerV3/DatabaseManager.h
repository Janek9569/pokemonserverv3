#pragma once
#include <string>
#include <functional>
#include "database.h"
#include "Item_t.h"

using DBResult_ptr = std::unique_ptr<DBReusltInterpeter>;

class DatabaseManager
{
public:
	DatabaseManager(std::unique_ptr<database> _db);

	bool beginTransaction() { return db->beginTransaction(); }
	bool rollback() { return db->rollback(); }
	bool commit() { return db->commit(); }

	DBResult_ptr getAllItemTypes();
	
private:
	std::unique_ptr<database> db;
};
