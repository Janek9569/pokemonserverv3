#pragma once
#include <vector>
#include <algorithm>
#include "SFML\System.hpp"

template<typename T>
class ThreadSafeVector
{
public:
	void push_back(T& v)
	{
		sf::Lock lock(mutex);
		vect.push_back(v);
	}

	void push_back(T&& v)
	{
		sf::Lock lock(mutex);
		vect.push_back(std::move(v));
	}

	T& operator[](size_t ind)
	{
		sf::Lock lock(mutex);
		return vect[ind];
	}

	const T& operator[](size_t ind) const
	{
		sf::Lock lock(mutex);
		return vect[ind];
	}

	size_t size()
	{
		sf::Lock lock(mutex);
		return vect.size();
	}

	template<class Function>
	Function for_each(Function fn)
	{
		sf::Lock lock(mutex);
		return std::for_each(vect.begin(), vect.end(), fn);
	}
private:
	sf::Mutex mutex;
	std::vector<T> vect;
};

