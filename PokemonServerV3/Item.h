#pragma once
#include <memory>
#include "Defines.h"
#include "UniqueObject.h"

class Server;

class Item;
typedef std::unique_ptr<Item> Item_ptr;

class Item : public UniqueObject
{
public:
	Item(Server* srv, uint32_t ID, uint32_t typeID, uint16_t count);
	Item() : srv(nullptr){}
	~Item();
	void serialize(ByteVector& data) const;
	const uint8_t* deserialize(Server* _srv, const uint8_t* data);

	uint32_t getTypeID() const;
	uint16_t getCount() const;
	void setCount(uint16_t newCount);

	static constexpr uint32_t NO_ITEM_TYPE_ID = 0xFFFFFFFFUL;
private:
	uint32_t ID;
	uint32_t typeID;
	uint16_t count;

	Server* srv;

	bool isInitialised() const { return srv != nullptr; }
};

